-- SUMMARY --

This module provides a VAT Number field that can be used in general.
The VAT will be validated against the VAT Information Exchange System.
Information on the EU VAT number checking service


For a full description of the module, visit the project page:
  http://drupal.org/project/vat_number
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/vat_number


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

None.


-- CUSTOMIZATION --

None.


-- TROUBLESHOOTING --

No known issues.


-- FAQ --

None.


-- CONTACT --

Current maintainers:
* Fido van den Bos (fidodido06) - https://www.drupal.org/u/fidodido06

This project has been sponsored by:
* Ordina Digital Services
  Specialized in Drupal development and maintenance.
