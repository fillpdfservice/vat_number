<?php

namespace Drupal\vat_number\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\vat_number\Controller\vatNumberController;

/**
 * Plugin implementation of the 'field_example_colorpicker' widget.
 *
 * @FieldWidget(
 *   id = "vat_widget",
 *   module = "vat_number",
 *   label = @Translation("VAT Number"),
 *   field_types = {
 *     "vat_number"
 *   }
 * )
 */
class VatNumberWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element += [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->getValue()['value']) ? $items[$delta]->getValue()['value'] : NULL,
      '#element_validate' => [
        [$this, 'validate'],
      ],
    ];

    return ['value' => $element];
  }

  /**
   * Validate the fields and check if the vat number is valid.
   */
  public function validate(&$element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (!empty($value)) {

      // Include the VAT Controller.
      $vatController = new VatNumberController($value);

      // Validate VAT number.
      $valid = $vatController->check();
      if (!$valid['status']) {
        $form_state->setError($element, $valid['message']);
        return;
      }

      // Temporarily store the timestamp and response on the element. We will
      // make use of this in massageFormValues().
      $value_element_path = \array_slice($element['#parents'], 0, -1);
      $timestamp_path = array_merge($value_element_path, ['timestamp']);
      $form_state->setValue($timestamp_path, time());
      $vies_response_path = array_merge($value_element_path, ['vies_response']);
      $form_state->setValue($vies_response_path, print_r($valid['response'], TRUE));
    }
  }

}
