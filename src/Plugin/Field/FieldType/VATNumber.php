<?php

namespace Drupal\vat_number\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemInterface;

/**
 * Provides a field type of vatNumber.
 *
 * @FieldType(
 *   id = "vat_number",
 *   label = @Translation("VAT Number"),
 *   default_formatter = "vat_formatter",
 *   default_widget = "vat_widget",
 * )
 */
class VatNumber extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'medium',
          'not null' => FALSE,
        ],
        'timestamp' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ],
        'vies_response' => [
          'type' => 'text',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')->setLabel(t('VAT Number'));
    $properties['timestamp'] = DataDefinition::create('integer')->setLabel('The timestamp when the response was received.');
    $properties['vies_response'] = DataDefinition::create('string')->setLabel('The raw SOAP response received from VIES.');
    return $properties;
  }

}
